class CreateFeedbacks < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
      t.string :body
      t.boolean :is_positive
      t.references :user, index: true
      t.references :item, index: true

      t.timestamps
    end
  end
end
