class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :body
      t.references :item, index: true
      t.references :user, index: true

      t.timestamps
    end
  end
end
