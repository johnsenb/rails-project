class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :name
      t.integer :price
      t.string :description
      t.string :image_url
      t.references :category, index: true
      t.references :gallery, index: true

      t.timestamps
    end
  end
end
