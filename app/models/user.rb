class User < ActiveRecord::Base
    has_many :galleries
    has_many :questions
    has_many :answers
end
