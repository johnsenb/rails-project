class Gallery < ActiveRecord::Base
  belongs_to :user
  belongs_to :county
  has_many :items
end
