class Item < ActiveRecord::Base
  belongs_to :category
  belongs_to :gallery
  has_many :questions
end
