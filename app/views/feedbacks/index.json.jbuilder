json.array!(@feedbacks) do |feedback|
  json.extract! feedback, :id, :body, :is_positive, :user_id, :item_id
  json.url feedback_url(feedback, format: :json)
end
