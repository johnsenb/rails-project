json.array!(@items) do |item|
  json.extract! item, :id, :name, :price, :description, :image_url, :category_id, :gallery_id
  json.url item_url(item, format: :json)
end
